#include "headfile.h"
uint16 adc_da;

void main()
{
	delay_for_ms(1000);
	TMOD = 0x01;
	pit0_init_ms(1);
	EA = 1;

	while (1)
	{
		adc_da = read_ad_data(OTHER);
		display_led(adc_da);
		delay_for_ms(50);
	}
}