#ifndef _headfile_
#define _headfile_

/*系统头文件 */
#include <reg52.h>
#include "intrins.h"
/*自定义头文件 */
#include "common.h"
#include "irq.h"
#include "button_led.h"
#include "uart.h"
#include "iic.h"
#include "spi.h"
#include "wifi.h"

#endif