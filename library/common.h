#ifndef _common_
#define _common_
#include "headfile.h"

/*单片机配置 */
#define STC_MHZ 11.0592

/*类型声名 */
typedef unsigned char uint8;
typedef unsigned short int uint16;
typedef unsigned long int uint32;

typedef char int8;
typedef short int int16;
typedef long int int32;

/*数码管数字 */
extern uint8 code table[];

#endif