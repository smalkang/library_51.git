#ifndef _uart_
#define _uart_

#include "headfile.h"

#define BAUDRATE_1 9600 //设置波特率

void uart_init(uint8 r);
void uart_putchar(uint8 data_sbuf);
void uart_sendbuff(uint8 *buff);
void uart_sendware(int16 *data_computer, uint16 length);
uint8 hc_165_in();
void hc595_out(uint8 data_temp);

#endif