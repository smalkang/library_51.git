#include "headfile.h"

/*******************************************************************
 WIFI初始化程序
 ********************************************************************/
void wifi_init(uint8 cwmode)
{
    uint8 wifi_ok = 0;
    if (cwmode == 2) // AP服务端
    {
        uart_init(1);
        EA = 1; //开总中断
        /*发送AT命令*/
        delay_for_ms(2000);
        uart_sendbuff("AT+RST\r\n");
        delay_for_ms(2000);

        uart_sendbuff("AT+CWMODE=2\r\n"); //设置AP工作模式
        delay_for_ms(2000);

        uart_sendbuff("AT+CWSAP=\"kang_51\",\"17393160419\",3,4\r\n"); //设置WiFi名称和密码\"在字符串中表示"
        delay_for_ms(2000);

        uart_sendbuff("AT+CIPMUX=1\r\n");
        delay_for_ms(2000);

        uart_sendbuff("AT+CIPSERVER=1,1234\r\n");
        delay_for_ms(2000);
    }
}

/*******************************************************************
 WIFI发送程序
 ********************************************************************/
void wifi_send(uint8 *wifidata, uint16 length)
{
    uint8 data_temp[25];
    /*sprintf(data_temp, "AT+CIPSEND=0,%d\r\n", length * 2);
    uart_sendbuff(data_temp);*/
    uart_sendbuff("AT+CIPSEND=0,2\r\n");
    delay_for_ms(20);
    uart_sendbuff(wifidata);
}
