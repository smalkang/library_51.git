#ifndef _button_
#define _button_

#include "headfile.h"

uint8 button_one_scanf();
uint8 button_matrix_scanf();
void display_led(uint16 num);

extern uint8 key_num;

#endif