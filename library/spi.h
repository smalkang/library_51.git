#ifndef _spi_
#define _spi_
#include "headfile.h"

/*对于HC6800-EM3系列开发板，ADC模块用的2046芯片,以下为该开发板对应的寄存器地址*/
#define DIANWEIQI 0x94
#define REMINDIANZU 0xd4
#define GUANGMINDIANZU 0xa4
#define OTHER 0xe4

uint16 read_ad_data(uint8 cmd);

#endif