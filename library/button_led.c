#include "headfile.h"

/*****************************************
数码管段显示 
默认P1^0口控制
******************************************/
/*段选 */
#define PIN_DISPLAY P0
sbit p10 = P1 ^ 0;
/*位选 */
sbit pc = P1 ^ 5;
sbit pb = P1 ^ 6;
sbit pa = P1 ^ 7;
void display_led(uint16 num)
{
	//个位显示
	p10 = 0;
	PIN_DISPLAY = table[num % 10];
	p10 = 1;
	pc = 1;
	pb = 1;
	pa = 1;
	delay_for_ms(2);
	if (num / 10)
	{
		//十位显示
		p10 = 0;
		PIN_DISPLAY = table[(num / 10) % 10];
		p10 = 1;
		pc = 1;
		pb = 1;
		pa = 0;
		delay_for_ms(2);
		if (num / 100)
		{
			//百位显示
			p10 = 0;
			PIN_DISPLAY = table[(num / 100) % 10];
			p10 = 1;
			pc = 1;
			pb = 0;
			pa = 1;
			delay_for_ms(2);
			if (num / 1000)
			{
				//千位显示
				p10 = 0;
				PIN_DISPLAY = table[(num / 1000) % 10];
				p10 = 1;
				pc = 1;
				pb = 0;
				pa = 0;
				delay_for_ms(2);
				if (num / 10000)
				{
					//万位显示
					p10 = 0;
					PIN_DISPLAY = table[(num / 10000) % 10];
					p10 = 1;
					pc = 0;
					pb = 1;
					pa = 1;
					delay_for_ms(2);
				}
			}
		}
	}
}

/****************************按下靠等待延时进行消抖，优点：简单；缺点：消耗资源，不适合精度要求高的工程********************************/
/****************************************
独立按键扫描程序
按键按下返回1，否则返回0
本按键为接地
*****************************************/
sbit P_key = P1 ^ 2;
uint8 button_one_scanf()
{
	P_key = 1;
	if (P_key == 0)
	{
		delay_for_ms(10); //消抖
		if (P_key == 0)
		{
			while (P_key == 0)
				; //按键回弹记为一次
			return 1;
		}
		else
			return 0;
	}
	else
		return 0;
}

/*****************************************
矩阵按键
先设置好管脚，避免冲突
 ******************************************/
#define PIN_BUTTON_MAT P0
uint8 time_button_mat = 0;
uint8 button_matrix_scanf()
{
	uint8 key_num = 0;
	time_button_mat = 0;
	PIN_BUTTON_MAT = 0xf0;
	if (PIN_BUTTON_MAT != 0XF0)
	{
		delay_for_ms(10);
		/*行发送高电平，列低电平，根据检测行是否为低电平确定行 */
		if (PIN_BUTTON_MAT != 0xf0)
		{
			switch (PIN_BUTTON_MAT)
			{
			case 0X70: //第一行
				key_num = 0;
				break;
			case 0Xb0:
				key_num = 4;
				break;
			case 0Xd0:
				key_num = 8;
				break;
			case 0Xe0:
				key_num = 12;
				break;
			}
		}
		/*列发送高电平，行为低电平，确定列 */
		PIN_BUTTON_MAT = 0x0f;
		if (PIN_BUTTON_MAT != 0x0f)
		{
			switch (PIN_BUTTON_MAT)
			{
			case 0x07:
				key_num += 0;
				break;
			case 0x0b:
				key_num += 1;
				break;
			case 0x0d:
				key_num += 2;
				break;
			case 0x0e:
				key_num += 3;
				break;
			}
		}
		while (time_button_mat < 10 && PIN_BUTTON_MAT != 0X0F)
		{
			delay_for_ms(10);
			time_button_mat++;
		}
	}
	return key_num;
}

/*******************************************靠定时器累加计时，等待时间内执行其他程序，时间到再判断按键******************************************* */