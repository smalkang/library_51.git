#include "headfile.h"

/***********************************************************
 串口发送/接收函数初始化
 只发送时r=0,接收时r=1
同时开的有定时器T0时需要重新设置TMOD
 ***********************************************************/
void uart_init(uint8 r)
{
    TMOD = 0x20;
    TH1 = 256 - STC_MHZ * 1000000 / 32 / BAUDRATE_1 / 12; //计时器工作方式为2，八位，则TH1与TL1相同
    TL1 = 256 - STC_MHZ * 1000000 / 32 / BAUDRATE_1 / 12;
    TR1 = 1;
    SM0 = 0;
    SM1 = 1;
    REN = r;
    ES = 1;
}

/*****************************************************
 uart发送单个字节
 管脚为该开发板固定的RX:P3.0,TX:P3.1
 *******************************************************/
void uart_putchar(uint8 data_sbuf)
{
    SBUF = data_sbuf;
    while (TI == 0)
        ; //等待发送完成
    TI = 0;
}

/*****************************************************
 uart发送数组
 uart_sendbuff(&arr[0])
 *******************************************************/
void uart_sendbuff(uint8 *buff)
{
    while ((*buff) != '\0')
    {
        uart_putchar(*buff);
        buff++;
    }
}

/******************************************************
 山外上位机发送波形图 
 默认发送int16型整数
 uart_sendware(&arr[0],sizeof(arr))
 *******************************************************/
void uart_sendware(int16 *data_computer, uint16 length)
{
    uint8 i = 0;
    uart_putchar(3); //山外上位机起始位
    uart_putchar(~3);

    for (i = 0; i < length; i++) //默认先发送低位，再发送高位。而不是顺序发送，所以需要变换
    {
        uart_putchar((uint8)data_computer[i / 2]);
        i++;
        uart_putchar((uint8)(data_computer[i / 2] >> 8));
    }

    uart_putchar(~3);
    uart_putchar(3); //山外上位机终止位
}

/******************************************************
 获取输入扩展口数据
 芯片74hc165
 *******************************************************/
sbit data_165 = P1 ^ 7;
sbit clk_165 = P3 ^ 6;
sbit s_l = P1 ^ 6;
uint8 hc_165_in()
{
    uint8 i = 8;
    uint8 data_temp;

    s_l = 0;
    data_temp = 0; //置零，延时
    s_l = 1;

    while (i--)
    {
        clk_165 = 0;
        data_temp <<= 1;
        data_temp |= data_165;
        clk_165 = 1;
    }

    return data_temp;
}

/******************************************************
 获取输出扩展口数据
 芯片74hc595
 *******************************************************/
sbit data_595 = P3 ^ 4;
sbit clk_595 = P3 ^ 6;     //输入时钟脉冲
sbit display_595 = P3 ^ 5; //上升沿触发并输出
void hc595_out(uint8 data_temp)
{
    uint8 i = 8;
    display_595 = 0;
    while (i--)
    {
        clk_595 = 0;
        data_595 = (data_temp & (1 << i));
        _nop_();
        clk_595 = 1;
    }
    display_595 = 1; //上升沿有效
}