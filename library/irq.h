#ifndef _irq_
#define _irq_

#include "headfile.h"


#define S(x) x * 3686.4//定时器工作方式2，3时单位换算

void delay_for_ms(int ms);
void pit0_init_ms(int ms);
void pit1_init_ms(int ms);

extern uint8 th0_val, th1_val;
extern uint8 tl0_val, tl1_val;
extern uint16 time3_T1;
extern uint16 time3_T0;
extern uint16 time2_T1;
extern uint16 time2_T0;

extern uint8 flag_1;
extern uint8 flag_2;

#endif