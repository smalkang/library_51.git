#ifndef _wifi_
#define _wifi_

#include "headfile.h"

void wifi_init(uint8 cwmode);
void wifi_send(uint8 *wifidata, uint16 length);

#endif