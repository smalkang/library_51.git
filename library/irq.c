#include "headfile.h"
uint8 flag_1 = 0;
uint8 flag_2 = 0;

/*********************************************************
delay_for_ms(50):延时50ms   
**********************************************************/
void delay_for_ms(int ms)
{
    int i;
    int j;
    for (i = 0; i < ms; i++)
        for (j = 0; j < 115; j++)
            ;
}

/********************************************************
pit0_init_ms(50):定时器0初始化，定时50ms 
工作方式1时ms最大值71；
工作方式0时ms最大为8；
工作方式为2和3时，此处设置ms没用。默认执行一次周期为1/3686s
想要定时时间长，可以搭配标志位
**********************************************************/
uint8 th0_val, tl0_val;
uint16 time3_T0 = 0;
uint16 time3_T1 = 0;
uint16 time2_T0 = 0;
uint16 time2_T1 = 0;
void pit0_init_ms(int ms)
{
    if ((TMOD & 0x03) == 0x00) //工作方式0
    {
        th0_val = (int)(8192 - (float)ms / 12 * STC_MHZ * 1000) / 32;
        tl0_val = (int)(8192 - (float)ms / 12 * STC_MHZ * 1000) % 32;
        TH0 = th0_val;
        TL0 = tl0_val;
        ET0 = 1; //开定时器中断
        TR0 = 1; //启动中断
    }
    else if ((TMOD & 0x03) == 0x01) //工作方式1
    {
        th0_val = (int)(65536 - (float)ms / 12 * STC_MHZ * 1000) / 256;
        tl0_val = (int)(65536 - (float)ms / 12 * STC_MHZ * 1000) % 256;
        TH0 = th0_val;
        TL0 = tl0_val;
        ET0 = 1; //开定时器中断
        TR0 = 1; //启动中断
    }
    else if ((TMOD & 0x03) == 0x02) //工作方式2
    {
        th0_val = 6;
        tl0_val = 6;
        TH0 = th0_val;
        TL0 = tl0_val;
        ET0 = 1;
        TR0 = 1;
    }
    else if ((TMOD & 0x03) == 0x03) //工作方式3
    {
        th0_val = 6;
        tl0_val = 6;
        TH0 = th0_val;
        TL0 = tl0_val;
        ET0 = 1;
        ET1 = 1;
        TR0 = 1;
        TR1 = 1;
    }
    else //TMOD设置有问题
        while (1)
            ;
}

/************************************************************
pit1_init_ms(50):定时器1初始化，定时50ms 
工作方式1时ms最大值71；
工作方式0时ms最大为8;
想要定时时间长，可以搭配标志位
 ************************************************************/
uint8 th1_val, tl1_val;
void pit1_init_ms(int ms)
{
    if ((TMOD & 0x30) == 0x00) //工作方式0
    {
        th1_val = (int)(8192 - (float)ms / 12 * STC_MHZ * 1000) / 32;
        tl1_val = (int)(8192 - (float)ms / 12 * STC_MHZ * 1000) % 32;
        TH1 = th1_val;
        TL1 = tl1_val;
        ET1 = 1; //开定时器中断
        TR1 = 1; //启动中断
    }
    else if ((TMOD & 0x30) == 0x10) //工作方式1
    {
        th1_val = (int)(65536 - (float)ms / 12 * STC_MHZ * 1000) / 256;
        tl1_val = (int)(65536 - (float)ms / 12 * STC_MHZ * 1000) % 256;
        TH1 = th1_val;
        TL1 = tl1_val;
        ET1 = 1; //开定时器中断
        TR1 = 1; //启动中断
    }
    else if ((TMOD & 0x30) == 0x20) //工作方式2
    {
        th1_val = 6;
        tl1_val = 6;
        TH1 = th1_val;
        TL1 = tl1_val;
        ET1 = 1;
        TR1 = 1;
    }
    else //TMOD设置有问题
        while (1)
            ;
}

/************************************************************
定时器0，1工作方式1中断调用函数 
适用工作方式0，1
工作方式3时：执行一次用时1/3686s，也可直接用s(1)代表1s
*************************************************************/
void T0_time() interrupt 1
{
    if ((TMOD & 0x03) == 0x01 || (TMOD & 0x03) == 0x00) //工作方式1,0
    {
        TH0 = th0_val;
        TL0 = tl0_val;
        /*中断执行代码：*/
        flag_1++;
    }
    else if ((TMOD & 0x03) == 0x02) //工作方式2
    {
        time2_T0++;
    }
    else if ((TMOD & 0x03) == 0x03) //工作方式3
    {
        TH0 = th0_val;
        time3_T0++;
    }
}
void T1_time() interrupt 3
{
    if ((TMOD & 0x30) == 0x10 || (TMOD & 0x30) == 0x00) //工作方式1,0
    {
        TH1 = th1_val;
        TL1 = tl1_val;
        /*中断执行代码：*/
        flag_2++;
    }
    else if ((TMOD & 0x30) == 0x20) //工作方式2
    {
        time2_T1++;
    }
    else if ((TMOD & 0x30) == 0x30) //工作方式3
    {
        TL0 = tl0_val;
        time3_T1++;
    }
}

/****************************************************************
 串口中断
 ******************************************************************/
void uart_irq() interrupt 4 //该函数执行条件：串口接收完一个字符
{
    RI = 0; //内部软件清零，接收到终止位自动置1
    flag_1 = 1;
}